package calculator;

public class Calculator {

    public double calcFahrenheit(double celsius) {
        return 1.8 * celsius + 32;
    }

}
