package calculator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Temperature extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Calculator calculator;

    public void init() throws ServletException {
        calculator = new Calculator();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Application";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in celsius: " +
                request.getParameter("temp") + "\n" +
                "  <P>Temperature in fahrenheit: " +
                Double.toString(calculator.calcFahrenheit(Double.parseDouble(request.getParameter("temp")))) + "<P>" +
                "<a href=\"http://localhost:8080/temperatureCalculator/\" class=\"button\">Calculate again</a>" +
                "</BODY></HTML>");
    }

}
